from biorobotics import PWM
from machine import Pin
from pin_definitions import Pins

def sign(num):
    if num >= 0:
        return 1
    else:
        return 0

def abs(num):
    if num >= 0:
        return num
    else:
        return -num

class Motor(object):
    def __init__(self, frequency):
        self.pwm = PWM(Pins.MOTOR_1_PWM, frequency)
        self.direct = Pin(Pins.MOTOR_1_DIRECTION, Pin.OUT)
        # self.pot = Pin(Pins.POTMETER_READ, Pin.IN)
    
    def write(self, strength):
        self.direct.value(sign(strength))
        self.pwm.write(abs(strength))
        #TODO: is direction correct?
