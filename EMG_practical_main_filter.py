from biorobotics import Ticker, AnalogIn, SerialPC

print('Starting EMG script...')


class biquad:
    def __init__(self, a1, a2, b0, b1, b2):
        self.a1 = a1
        self.a2 = a2
        self.b0 = b0
        self.b1 = b1
        self.b2 = b2
        self.w1 = 0.0
        self.w2 = 0.0

    def filter(self, x):
        y = float(self.b0*x + self.w1)
        self.w1 = float(self.b1*x - self.a1*y + self.w2)
        self.w2 = float(self.b2*x - self.a2*y)
        return y

# Initialize filters obtained from MATLAB's filterdesigner 
# 300Hz fs; 10Hz cut off Butterworth low pass
LP_100_10_1 = biquad(-1.705552145544083852968242354108951985836,   0.743655195048865791385139800695469602942, 1.0, 2.0, 1.0)
gain_1 = 0.009525762376195455113925270040908799274 
# 300Hz fs; 48-52 band stop Butterworth
LP_100_10_2 = biquad(-0.960616192564186621716260106040863320231,  0.919547137907040124105151335243135690689, 1.0, -1.000877940003687793790732030174694955349, 1.0)
gain_2 = 0.959773568953520062052575667621567845345 



pc = SerialPC(3)

emgs = [AnalogIn('A0'), AnalogIn('A1')]


def loop():
    
    for i, emg in enumerate(emgs):
        pc.set(i, emg.read())
        
    
    pc.set(2, gain_1*LP_100_10_1.filter(emg.read()))
    pc.send()
    
    



ticker = Ticker(0, 300, loop, enable_gc=True)

ticker.start()
