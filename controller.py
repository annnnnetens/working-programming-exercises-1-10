from filters import Biquad

class PID(object):
    # Used in state_machine and then in state_functions
    # currently constants defined in state_machine
    
    def __init__(self, ticker_frequency, k_p, k_d, k_i):

        
        self.time_step = 1 / ticker_frequency
        self.k_p = k_p
        self.k_d = k_d
        self.k_i = k_i

        self.past_error = 0
        self.integrated_error = 0

        return

    def step(self, reference, measured, filtfun=None):
        ### Both reference and measured are in ticks.
                

        if filtfun is not None:
            err = reference - measured
            error_p = filtfun.step(err)
        else:
            error_p = reference - measured
        error_d = (error_p - self.past_error)/self.time_step
        self.past_error = error_d
        self.integrated_error = self.integrated_error + error_p * self.time_step

        c_error = error_p * self.k_p + error_d * self.k_d + self.integrated_error *self.k_i
        
        return c_error