from motor import Motor
from states import State
from sensor import SensorState
from leds import Leds
from biorobotics import SerialPC
from motor import Motor
from controller import PID

class StateFunctions(object):
    # used in state_machine for callbacks

    def __init__(self, robot_state, sensor_state, pid, biquad, frequency):

        self.robot_state = robot_state
        self.sensor_state = sensor_state
        self.leds = Leds()
        self.motor = Motor(frequency)
        self.pc = SerialPC(1)
        self.pid = pid
        self.biquad = biquad

        self.callbacks = {
            State.SAFE: self.safe,
            State.READ: self.read,
            State.ON: self.on
        }
        return


    def safe(self):
        
        # Entry action
        if self.robot_state.is_changed():
            self.robot_state.set(self.robot_state.current)
            self.leds.green.on()
            print("state safe")
        # Main action
        self.motor.write(0)

        # State guards, with possible exit action
        if self.sensor_state.switch_value == 1:
            self.leds.green.off()
            self.robot_state.set(State.READ)



        return


    def read(self):
        # Entry action
        if self.robot_state.is_changed():
            self.robot_state.set(self.robot_state.current)
            self.leds.yellow.on()
            print("state read")
        # Main action
        
        print(self.sensor_state.potread)
        self.motor.write(0)

        self.sensor_state
        # State guards, with possible exit action
        if self.sensor_state.switch_value == 1:
            self.leds.yellow.off()
            self.robot_state.set(State.ON)

        return


    def on(self):

        # Entry action
        if self.robot_state.is_changed():
            self.robot_state.set(State.ON)
            self.leds.red.on()
            print("state on")

        # Main action
        reference = self.sensor_state.potread    
        measured = self.sensor_state.encoder_value
        
        print(self.sensor_state.encoder_value)
        # print(reference, measured)


        # ref is in [0, 1], but we need output in [0, 1]
        measured = measured / (64 * 131.25)

        # now measured is in [0, 1]
        c_error = self.pid.step(reference, measured, self.biquad)
        # c_error is in full circle 
        c_error = c_error * 64 * 131.25
        # now it is in ticks

        self.motor.write(c_error) # motor writing is in speed, and c_error is speed?

        # State guards, with possible exit action
        if self.sensor_state.switch_value == 1:
            self.leds.red.off()
            self.robot_state.set(State.SAFE)


        return
