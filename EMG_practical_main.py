from biorobotics import Ticker, AnalogIn, SerialPC

print('Starting EMG script...')

pc = SerialPC(2)

emgs = [AnalogIn('A0'), AnalogIn('A1')]


def loop():
    
    for i, emg in enumerate(emgs):
        pc.set(i, emg.read())
    
    pc.send()


ticker = Ticker(0, 250, loop, enable_gc=True)

ticker.start()
