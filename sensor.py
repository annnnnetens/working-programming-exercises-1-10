# import random
from switch import BlueSwitch
from pin_definitions import Pins
from biorobotics import AnalogIn, Encoder


class SensorState(object):

    def __init__(self):
        
        self.switch_value = 0
        self.blue_switch = BlueSwitch()

        self.potread = 0
        self.pin_potread = AnalogIn(Pins.POTMETER_READ)

        self.pins_encoder = Encoder(Pins.ENCODER_A, Pins.ENCODER_B)
        self.encoder_value = 0    # will be needed later on
        
        return

    def update(self):

        self.switch_value = self.blue_switch.value()
        self.potread = self.pin_potread.read()
        self.encoder_value = self.pins_encoder.counter()

        return

# Try this command in terminal:
# robot.sensor_state.blue_switch.switch.test_push()
        