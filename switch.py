from pyb import Switch
import biorobotics
import utime


class BlueSwitch(object):

    def __init__(self):
        self.switch_value = 0
        self.switch = Switch()
        self.switch.callback(self.callback)
        self.t1 = 0
        self.t2 = 0
        return


    def callback(self):
        
        # t2 = biorobotics.toc()
        # if t2 - t1 > 1000:
        self.switch_value = 1
            
        # return

        # t1 = biorobotics.tic()



    def value(self):
        return_value = self.switch_value
        self.switch_value = 0
        return return_value

        