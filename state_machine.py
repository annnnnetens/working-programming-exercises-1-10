
from biorobotics import Ticker, Encoder
from state_functions import StateFunctions
from states import State
from sensor import SensorState
from controller import PID
from filters import Biquad



class StateMachine(object):
    # one of the main files, runs the whole machine, launched from main

    def __init__(self, ticker_frequency):
        self.robot_state = State()
        self.sensor_state = SensorState()
        
        self.pid = PID(ticker_frequency, 1, 0, 0)
        self.biquad = Biquad(1, 2, 1, 1, 2, 1)
        
        self.state_functions = StateFunctions(self.robot_state, self.sensor_state, self.pid, self.biquad, ticker_frequency)

        self.ticker = Ticker(0, ticker_frequency, self.run)

        return


    def run(self):

        self.sensor_state.update()
        # self.sensor_state.blue_switch.callback()

        # print(self.sensor_state.switch_value)
        self.state_functions.callbacks[self.robot_state.current]()
        return

    def start(self):
        self.ticker.start()
        return

    def stop(self):
        self.ticker.stop()
        return

    

