

class State(object):
    # just updating states
    
    SAFE = 'safe'
    ON = 'on'
    READ = 'read'
    


    def __init__(self):

        self.current = self.READ
        self.previous = None

        return
    
    def is_changed(self):
        return self.previous is not self.current

    def set(self, state):
        self.previous = self.current
        self.current = state
        return



    


