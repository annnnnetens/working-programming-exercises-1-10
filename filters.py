

class Biquad(object):

    def __init__(self, a0, a1, a2, b0, b1, b2):
        a1, a2, b0, b1, b2 = a1/a0, a2/a0, b0/a0, b1/a0, b2/a0
        # now the coefficients are normalized

        self.a0 = 1
        self.a1 = a1
        self.a2 = a2

        self.b0 = b0
        self.b1 = b1
        self.b2 = b2
        
        self.delay_1 = 0
        self.delay_2 = 0
        
        self.delay_filt_1 = 0
        self.delay_filt_2 = 0
        
        return

    def step(self, sample):

        const1 = self.b0 * sample + self.b1 * self.delay_1 + self.b2 * self.delay_2
        const2 = self.a1 * self.delay_filt_1 + self.a2 * self.delay_filt_2


        better_sample = const1 - const2

        self.delay_2 = self.delay_1
        self.delay_1 = sample

        self.delay_filt_2 = self.delay_filt_1
        self.delay_filt_1 = better_sample

        return better_sample
    
